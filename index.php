
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lebrain - Maquetas </title>
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.15.0/css/mdb.min.css" rel="stylesheet">

    
    <!-- Local CNDS -->
    <!-- Bootstrap core CSS -->
    <!-- <link href="http://cdns.mdbbootstrap.home/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- Material Design Bootstrap -->
    <!-- <link href="http://cdns.mdbbootstrap.home/css/mdb.min.css" rel="stylesheet"> -->
</head>
<body class="">
    
    <nav class="navbar navbar-expand-lg scrolling-navbar top-nav-collapse bg-primary text-white">
        <div class="container">
            <!-- SideNav slide-out button -->
            <div class="float-left">
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i></a>
            </div>
            <!-- Breadcrumb-->
            <div class="breadcrumb-dn mr-md-auto mx-auto mx-md-3">
                <p class="my-0">Lebrain</p>
            </div>
            <div class="d-md-flex d-none change-mode">
                <!-- Navbar links -->
                <ul class="nav navbar-nav nav-flex-icons">
                    <li class="nav-item">
                        <a class="nav-link waves-effect"><i class="fas fa-envelope"></i> <span class="clearfix d-none d-sm-inline-block">Contactanos</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect"><i class="far fa-comments"></i> <span class="clearfix d-none d-sm-inline-block">Soporte</span></a>
                    </li>
                </ul>
                <!-- Navbar links -->
            </div>
        </div>
    </nav>




    <?php
        $directorio = opendir("."); //ruta actual
        while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
        {
            if (is_dir($archivo)){ }
            else {  
                
                $info = new SplFileInfo($archivo);
                switch ($info->getExtension()) {
                    case 'html':
                        $archivo_name[] = $archivo;
                        break;
                    case 'zip':
                        $descargar = $archivo;
                        break;
                    default:
                        # code...
                        break;
                }
            }
        }
    ?>
    <div class="container mt-5">
        <div class="d-flex">
            DM = Desktop y Movil
        </div>
        <div class="d-flex">
            M = Solo Movil
        </div>
        <p class="bg-warning">Esto porque habia pantallas que solo son para moviles</p>
    </div>
    <div class="container row mx-auto mt-5">
        <?php  foreach ($archivo_name as $key => $value) { ?>
            <div class="col-md-4 col-12 p-3 mx-auto">
                <!-- Card --> 
                <div class="card">
                    <!-- Card image -->
                    <div class="view overlay" style="height: 160px;" >
                        <a href="<?=$value?>" style="height: 237px;width: 278px;position: absolute;">  
                         <iframe id="livepreview_iframe" src="http://<?=$_SERVER['SERVER_NAME'].'/'.$value?>" style="height: 800px;width: 1400px;-moz-transform: scale(0.3);-moz-transform-origin: 0 0;-o-transform: scale(0.3);-o-transform-origin: 0 0;-webkit-transform: scale(0.242);-webkit-transform-origin: 0 0;">
                         </iframe>
                           
                        </a>
                    </div>
                    <!-- Card image -->
                    <!-- Card content -->
                    <div class="card-body">
                        <!-- Card footer -->
                        <div class="card-footer pb-0">
                            <div class="row mb-0">
                                <span class="float-left">
                                    <strong>
                                        <a href="<?=$value?>" target="_blank" >
                                            <?=$value?>
                                        </a>
                                    </strong>
                                </span>
                                <span class="float-right">
                                    <a class="" data-toggle="tooltip" data-placement="top" title="Add to Cart">
                                        <i class="fas fa-shopping-cart ml-3"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- Card content -->
                </div>
                <!-- Card -->
            </div>
        
        <?php } ?>
        <?php if ($descargar) { ?>
            <a class="btn btn-primary my-5 mx-auto" href="<?=$descargar?>">Descargar proyecto <?=$descargar?></a>
        <?php }?>
    </div>
</body>
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.15.0/js/mdb.min.js"></script>
        
    <!-- Local CDNS -->
    <!-- JQuery -->
    <!-- <script type="text/javascript" src="http://cdns.mdbbootstrap.home/js/jquery.min.js"></script> -->
    <!-- Bootstrap tooltips -->
    <!-- <script type="text/javascript" src="http://cdns.mdbbootstrap.home/js/popper.min.js"></script> -->
    <!-- Bootstrap core JavaScript -->
    <!-- <script type="text/javascript" src="http://cdns.mdbbootstrap.home/js/bootstrap.min.js"></script> -->
    <!-- MDB core JavaScript -->
    <!-- <script type="text/javascript" src="http://cdns.mdbbootstrap.home/js/mdb.min.js"></script> -->

</html>









