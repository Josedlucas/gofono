$('input:checkbox').on('click', function (e) {
    $(this).attr("checked", this.checked);
    if ($(this).attr("checked")) {
        var labelText = $("label[for='"+$(this).attr("id")+"']").text();
        var icon = '<svg class="bi bi-x-circle-fill align-items-center ml-2" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M16 8A8 8 0 110 8a8 8 0 0116 0zm-4.146-3.146a.5.5 0 00-.708-.708L8 7.293 4.854 4.146a.5.5 0 10-.708.708L7.293 8l-3.147 3.146a.5.5 0 00.708.708L8 8.707l3.146 3.147a.5.5 0 00.708-.708L8.707 8l3.147-3.146z" clip-rule="evenodd"/></svg>';
        $('.tag-filter').append('<div class="text-muted-GoFono mt-2 ml-2 remover-tag" data-id="'+$(this).attr("id")+'">'+ labelText + icon+'</div>' );
    }else{

    }
});
$(document).on('click', '.remover-tag', function (e) {
    $("label[for='"+$(this).data("id")+"']").click();
    $(this).remove();
});