function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        zoomControl:false,
        streetViewControl:true,
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
    map.setTilt(50);
        
    // Multiple markers location, latitude, and longitude
    var markers = [
        ['GoFono ', 37.770985, -122.4123258],
        ['GoFono', 37.7710647, -122.4047753],
        ['Prospect Park Zoo, NY', 37.7684428, -122.4011901]
    ];
                        
    // Info window content
    var infoWindowContent = [
        ['<div class="info_content">' +
        '<h3>GoFono Tienda Numero 1</h3>' +
        '<p>Esta es la tienda Numero 1 de GoFono San Francisco</p>' + '</div>'],
        ['<div class="info_content">' +
        '<h3>GoFono Tienda Numero 2</h3>' +
        '<p>Esta es la tienda Numero 2 de GoFono San Francisco</p>' +
        '</div>'],
        ['<div class="info_content">' +
        '<h3>GoFono Tienda Numero 2</h3>' +
        '<p>Esta es la tienda Numero 2 de GoFono San Francisco.</p>' +
        '</div>']
    ];
        
    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Place each marker on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            icon:'../img/logo-maps.png',
            title: markers[i][0]
        });
        
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
           
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
           
        })(marker, i));

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(15);
        google.maps.event.removeListener(boundsListener);
    });
    
}
// Load initialize function
google.maps.event.addDomListener(window, 'load', initMap);